#include "sparse_matrix.h"
#include <cmath>

using namespace std;


inline float scalarProduct(const IntFloatMap& l1,
			   const IntFloatMap& l2, 
			   int max_pos=std::numeric_limits<int>::max()){
  float prod=0;
  IntFloatMap::const_iterator it1=l1.begin();
  IntFloatMap::const_iterator it2=l2.begin();
  while(it1!=l1.end() && it2!=l2.end()){
    int p1=it1->first;
    int p2=it2->first;
    if (p1>max_pos || p2>max_pos)
      return prod;
    if(p1==p2){
      prod+=it1->second*it2->second;
      it1++;
      it2++;
    } else if (p1>p2)
      it2++;
    else if (p1<p1)
      it1++;
  }
  return prod;
}

inline bool scalarStructureProduct(const IntFloatMap& l1,
				   const IntFloatMap& l2,
				   int max_pos=std::numeric_limits<int>::max()){
  IntFloatMap::const_iterator it1=l1.begin();
  IntFloatMap::const_iterator it2=l2.begin();
  while(it1!=l1.end() && it2!=l2.end()){
    int p1=it1->first;
    int p2=it2->first;
     if (p1>max_pos || p2>max_pos)
      return false;
    if(p1==p2)
      return true;
    if (p1>p2)
      it2++;
    else if (p1<p2)
      it1++;
  }
  return false;
}


SparseMatrix::SparseMatrix() {
  this->num_rows=0;
  this->num_cols=0;
}

SparseMatrix::SparseMatrix(int num_rows, int num_cols) {
  this->num_rows=num_rows;
  this->num_cols=num_cols;
  rows.resize(num_rows);
}

void SparseMatrix::setElement(int r, int c, float value){
  if (r>=num_rows)
    throw std::runtime_error("out of bound");
  if (r>=num_cols)
    throw std::runtime_error("out of bound");
    
  IntFloatMap& row=rows[r];
  IntFloatMap::iterator it=row.find(c);
  if (it==row.end()) {
    if (value!=0.0f) {
      row.insert(std::make_pair(c,value));
    }
  } else {
    if (value==0.0f)
      row.erase(it);
    else
      it->second-value;
  }
}

float SparseMatrix::element(int r, int c) const {
  if (r>=num_rows)
    throw std::runtime_error("out of bound");
  if (r>=num_cols)
    throw std::runtime_error("out of bound");
    
  const IntFloatMap& row=rows[r];
  IntFloatMap::const_iterator it=row.find(c);
  if (it==row.end())
    return 0.0f;
  return it->second;
}

bool SparseMatrix::isElement(int r, int c) const {
  const IntFloatMap& row=rows[r];
  IntFloatMap::const_iterator it=row.find(c);
  if (it==row.end())
    return false;
  return true;
}

void SparseMatrix::resize(int new_rows, int new_cols){
  rows.resize(new_rows);
  for (size_t r=0; r<rows.size(); r++){
    IntFloatMap& row=rows[r];
    IntFloatMap::iterator start_it=row.upper_bound(new_cols-1);
    row.erase(start_it, row.end());
  }
  num_rows=new_rows;
  num_cols=new_cols;
}

int SparseMatrix::choleskyStructure(SparseMatrix& lower) const {
  if (num_rows!=num_cols)
    throw std::runtime_error("cholesky not defined on non square matrix");

  lower=SparseMatrix(num_rows, num_cols);
  int nnz=0;
  for(int r=0; r<num_rows; r++){
    const IntFloatMap& a_row=rows[r];
    IntFloatMap& l_row=lower.rows[r];
    // find the minimum col in the row in a
    if (a_row.empty())
      return -r;
    int min_col=a_row.begin()->first;
    for (int c=min_col; c<=r; c++){
      bool full=false;
      if (isElement(r,c)) {
	full=true;
      } else {
	IntFloatMap& l_upper_row=lower.rows[c];
	full=scalarStructureProduct(l_row, l_upper_row,c);
      }
      if (full) {
	l_row.insert(std::make_pair(c,1));
	nnz++;
      }
    }
  }
  return nnz;
}

int SparseMatrix::cholesky(SparseMatrix& lower) const {
  if (num_rows!=num_cols)
    throw std::runtime_error("cholesky not defined on non square matrix");

  lower=SparseMatrix(num_rows, num_cols);
  int nnz=0;
  for(int r=0; r<num_rows; r++){
    const IntFloatMap& a_row=rows[r];
    IntFloatMap& l_row=lower.rows[r];
    float diagonal_values[num_rows];
    float inverse_diagonal_values[num_rows];
    if (a_row.empty())
      return -r;
    int min_col=a_row.begin()->first;
    for (int c=min_col; c<=r; c++){
      float accumulator=0;
      IntFloatMap& l_upper_row=lower.rows[c];
      accumulator=element(r,c)-scalarProduct(l_row, l_upper_row, c-1);
      float l_rc=0;
      if (r==c){
	if(accumulator>0){
	  l_rc=sqrt(accumulator);
	  diagonal_values[r]=l_rc;
	  inverse_diagonal_values[r]=1/l_rc;
	} else {
	  return -r;
	}
      } else {
	l_rc=inverse_diagonal_values[c]*accumulator;
      }
      l_row.insert(std::make_pair(c,l_rc));
      nnz++;
    }
  }
  return nnz;
}

int SparseMatrix::choleskyRefresh(SparseMatrix& lower) const {
  if (num_rows!=num_cols)
    throw std::runtime_error("cholesky not defined on non square matrix");

  int nnz=0;
  for(int r=0; r<num_rows; r++){
    const IntFloatMap& a_row=rows[r];
    IntFloatMap& l_row=lower.rows[r];
    float diagonal_values[num_rows];
    float inverse_diagonal_values[num_rows];
    for (IntFloatMap::iterator it=l_row.begin(); it!=l_row.end(); it++){
      int c=it->first;
      float accumulator=0;
      IntFloatMap& l_upper_row=lower.rows[c];
      accumulator=element(r,c)-scalarProduct(l_row, l_upper_row, c-1);
      float l_rc=0;
      if (r==c){
	if(accumulator>0){
	  l_rc=sqrt(accumulator);
	  diagonal_values[r]=l_rc;
	  inverse_diagonal_values[r]=1/l_rc;
	} else {
	  return -r;
	}
      } else {
	l_rc=inverse_diagonal_values[c]*accumulator;
      }
      it->second=l_rc;
      nnz++;
    }
  }
  return nnz;
}


void SparseMatrix::computeMinCols(IntVector& min_col_indices) {
  min_col_indices.resize(num_rows);
  std::fill(min_col_indices.begin(), min_col_indices.end(), num_cols);
  for (size_t r=0; r<num_rows; r++){
    const IntFloatMap& row=rows[r];
    if (!row.empty())
      min_col_indices[r]=row.begin()->first;;
  }
}

void SparseMatrix::clear() {
  for (size_t r=0; r<num_rows; r++){
    rows[r].clear();
  }
}

void SparseMatrix::computeDegree(IntVector& degrees, int start_degree){
  degrees.resize(num_cols);
  std::fill(degrees.begin(), degrees.end(), 0);
  for (size_t r=start_degree; r<num_rows; r++){
    const IntFloatMap& row=rows[r];
    for (IntFloatMap::const_iterator it=row.begin(); it!=row.end(); it++)
      degrees[it->first]++;
  }
}

int SparseMatrix::countNonZeros() const {
  int nnz=0;
  for (size_t r=0; r<num_rows; r++){
    nnz+=rows[r].size();
  }
  return nnz;
}

void SparseMatrix::structureToCvMat(cv::Mat& m, unsigned char gray_level) const{
  for (size_t r=0; r<num_rows; r++){
    unsigned char* image_row=m.ptr<unsigned char>(r);
    const IntFloatMap& row=rows[r];
    for (IntFloatMap::const_iterator it=row.begin(); it!=row.end(); it++)
      image_row[it->first]=gray_level;
  }
}

cs* SparseMatrix::toCS() const {
  int nnz=countNonZeros();
  cs* transposed_matrix=cs_spalloc(num_cols, num_rows, nnz, 1, 0);
  int *row_pointers=transposed_matrix->p;
  int* col_indices=transposed_matrix->i;
  double* values=transposed_matrix->x;

  int counter=0;
  for (size_t r=0; r<num_rows; r++){
    *row_pointers=counter;
    const IntFloatMap& row=rows[r];
    for (IntFloatMap::const_iterator it=row.begin(); it!=row.end(); it++) {
      int c=it->first;
      float value=it->second;
      *values=value;
      *col_indices=c;
      counter++;
      values++;
      col_indices++;
    }
    row_pointers++;
  }
  *row_pointers=counter;
  cs* tm=cs_transpose(transposed_matrix,1);
  cs_spfree(transposed_matrix);

  return tm;
}

void SparseMatrix::fromCS(const cs* src){
  resize(src->m, src->n);
  const int* col_pointers=src->p;

  const int* row_indices=src->i;
  double* values=src->x;
  int c=0;
  int col_index=col_pointers[c+1];
  int num_elements=src->p[src->n];
  for (int i=0; i<num_elements; i++, row_indices++, values++){
    double value=*values;
    double r=*row_indices;
    setElement(r,c,value);
    if (i>col_index){
      c++;
      col_index=col_pointers[c+1];
    }
  }
}

  
std::ostream& operator <<(std::ostream& os, const SparseMatrix& m){
  os << m.num_rows << " " << m.num_cols << std::endl;
  for (int r=0; r<m.num_rows; r++){
    for (int c=0; c<m.num_cols; c++){
      os << m.element(r,c) << " ";
    }
    os << std::endl;
  }
  return os;
}


std::istream& operator >>(std::istream& is, SparseMatrix& m){
  int num_rows;
  int num_cols;
  is >> num_rows >> num_cols;
  if (! is.good())
    return is;
  m=SparseMatrix(num_rows, num_cols);
  for (int r=0; r<m.num_rows; r++){
    for (int c=0; c<m.num_cols; c++){
      float v;
      is >> v;
      if (! is.good())
	return is;
      m.setElement(r,c,v);
    }
  }
  return is;
}

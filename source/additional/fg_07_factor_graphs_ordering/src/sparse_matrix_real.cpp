#include <iostream>
#include <vector>
#include <map>
#include <cstdlib>
#include <cmath>
#include <stdexcept>
#include <limits>
#include <algorithm>
#include "opencv2/opencv.hpp"

#include "sparse_matrix.h"
#include "factor.h"
#include "int_vector.h"
#include "orderings.h"
#include "g2o_file_reader.h"

#include "g2o/types/slam2d/vertex_se2.h"
#include "g2o/types/slam2d/edge_se2.h"


using namespace std;

g2o::VertexSE2 v;
g2o::EdgeSE2 e;

inline void fillMatrix(SparseMatrix& dest,
                       const FactorVector& factors,
                       size_t num_variables,
                       const IntVector& permutation=IntVector())
{
    dest.clear();
    dest.resize(num_variables, num_variables);
    for (size_t i=0; i<factors.size(); i++){
        const Factor& factor=factors[i];
        int from_idx=factor.from;
        int to_idx=factor.to;
        if(permutation.size()){
            from_idx=permutation[from_idx];
            to_idx=permutation[to_idx];
        }
        int a=std::min(from_idx, to_idx);
        int b=std::max(from_idx, to_idx);
        dest.setElement(a,a,1);
        dest.setElement(a,b,1);
        dest.setElement(b,a,1);
        dest.setElement(b,b,1);
    }
}


typedef std::set<int> VariablesSet;

int main(int argc, char** argv)
{
    bool do_plain = true;
    bool do_retard = true;
    bool do_space = false;
    bool do_mdo = true;

    if(argc != 2)
    {
        cerr << "[ERROR]: Expecting ./sparse_matrix_real <input graph>" << endl;
        return -1;
    }

    G2OFileReader file_reader(argv[1]);
    file_reader.extractVariablesAndFactors();

    cv::namedWindow("plain");
    cv::namedWindow("retard");
    cv::namedWindow("space");
    cv::namedWindow("mdo");
    int variables = file_reader.numVariables();
    cv::Mat plain_system_image(variables, variables, CV_8UC1);
    cv::Mat retard_system_image(variables, variables, CV_8UC1);
    cv::Mat mdo_system_image(variables, variables, CV_8UC1);
    cv::Mat space_system_image(variables, variables, CV_8UC1);

    FactorVector& factors = file_reader.factors();
    int steps = factors.size();
    FactorVector current_factors;
    VariablesSet unique_variables;
    int num_variables = 0;
    for(int i = 0; i < steps; ++i)
    {
        current_factors.push_back(factors[i]);

        unique_variables.insert(factors[i].from);
        unique_variables.insert(factors[i].to);
        num_variables = unique_variables.size();

        IntVector plain_min_col, plain_degree;
        SparseMatrix plain_matrix;
        fillMatrix(plain_matrix, current_factors, num_variables);
        plain_matrix.computeMinCols(plain_min_col);
        plain_matrix.computeDegree(plain_degree);

        cerr << "Factors generated: " << current_factors.size()
             << " Num_variables: " << num_variables << endl;


        /***************  PLAIN VERSION ************************/
        if(do_plain)
        {
            cerr << "********* PLAIN **********" << endl;
            cerr << "nnz:" << plain_matrix.countNonZeros() << endl;
            plain_system_image = cv::Scalar(255);
            plain_matrix.structureToCvMat(plain_system_image, 0);
            SparseMatrix plain_chol;
            plain_matrix.choleskyStructure(plain_chol);
            cerr << "chol nnz:" << plain_chol.countNonZeros() << endl;
            plain_chol.structureToCvMat(plain_system_image, 125);
            cv::imshow("plain", plain_system_image);
        }


        /***************  RETARD VERSION ************************/
        if(do_retard)
        {
            IntVector retard_permutation;
            computeRetardedPermutation(retard_permutation, current_factors, num_variables);

            SparseMatrix retard_matrix;
            cerr << "********* RETARD **********" << endl;
            fillMatrix(retard_matrix, current_factors,num_variables,retard_permutation);
            cerr << "nnz:" << retard_matrix.countNonZeros() << endl;
            retard_system_image=cv::Scalar(255);
            retard_matrix.structureToCvMat(retard_system_image,0);
            SparseMatrix retard_chol;
            retard_matrix.choleskyStructure(retard_chol);
            cerr << "chol nnz:" << retard_chol.countNonZeros() << endl;
            retard_chol.structureToCvMat(retard_system_image,125);
            cv::imshow("retard", retard_system_image);
        }

//        if(do_space)
//        {
//            /***************  SPACE VERSION ************************/
//            IntVector space_permutation;
//            computeSpacePermutation(space_permutation, simulator.locations);

//            SparseMatrix space_matrix;
//            cerr << "********* SPACE **********" << endl;
//            fillMatrix(space_matrix,factors,num_variables,space_permutation);
//            cerr << "nnz:" << space_matrix.countNonZeros() << endl;
//            space_system_image=cv::Scalar(255);
//            space_matrix.structureToCvMat(space_system_image,0);
//            SparseMatrix space_chol;
//            space_matrix.choleskyStructure(space_chol);
//            cerr << "chol nnz:" << space_chol.countNonZeros() << endl;
//            space_chol.structureToCvMat(space_system_image,125);
//            cv::imshow("space", space_system_image);
//        }


        /***************  MDO VERSION ************************/
        if (do_mdo)
        {
            IntVector mdo_permutation;
            computeMDOPermutation(mdo_permutation, plain_degree);

            SparseMatrix mdo_matrix;
            cerr << "********* MDO **********" << endl;
            fillMatrix(mdo_matrix, current_factors, num_variables, mdo_permutation);
            cerr << "nnz:" << mdo_matrix.countNonZeros() << endl;
            mdo_system_image=cv::Scalar(255);
            mdo_matrix.structureToCvMat(mdo_system_image,0);
            SparseMatrix mdo_chol;
            mdo_matrix.choleskyStructure(mdo_chol);
            cerr << "chol nnz:" << mdo_chol.countNonZeros() << endl;
            mdo_chol.structureToCvMat(mdo_system_image,125);
            cv::imshow("mdo", mdo_system_image);
        }
        cv::waitKey(10);
    }
    cv::waitKey(0);
}


#pragma once
#include "factor.h"
#include "int_vector.h"
#include <algorithm>
#include "opencv2/opencv.hpp"

inline void cholAnalyze(IntVector& min_col,
		 IntVector& degree,
		 const FactorVector& factors, 
		 size_t num_variables,
		 const IntVector& permutation=IntVector()) {
  min_col.resize(num_variables);
  degree.resize(num_variables);
  for (size_t i=0; i<num_variables; i++) {
    int idx=i;
    if(permutation.size()){
      idx=permutation[idx];
    }
    min_col[idx]=idx;
  }
  std::fill(degree.begin(), degree.end(), 0);
  for (size_t i=0; i<factors.size(); i++){
    const Factor& factor=factors[i];
    int from_idx=factor.from;
    int to_idx=factor.to;
    if(permutation.size()){
      from_idx=permutation[from_idx];
      to_idx=permutation[to_idx];
    }
    int a=std::min(from_idx, to_idx);
    int b=std::max(from_idx, to_idx);
    degree[a]++;
    degree[b]++;
    if (min_col[b]>a)
      min_col[b]=a;

    if (min_col[a]>a)
      min_col[a]=a;

    if (min_col[a]>b)
      min_col[a]=b;
  }
}

inline void plotFactors(cv::Mat& image,
		 const FactorVector& factors, 
		 size_t num_variables,
		 const IntVector& permutation=IntVector()) {
  if (image.rows<num_variables||image.cols<num_variables)
    image.create(num_variables, num_variables, CV_8UC1);
  image=255;
  for (size_t i=0; i<factors.size(); i++){
    const Factor& factor=factors[i];
    int from_idx=factor.from;
    int to_idx=factor.to;
    if(permutation.size()){
      from_idx=permutation[from_idx];
      to_idx=permutation[to_idx];
    }
    int a=std::min(from_idx, to_idx);
    int b=std::max(from_idx, to_idx);
    image.at<unsigned char>(b,a)=0;
    image.at<unsigned char>(a,a)=0;
    image.at<unsigned char>(b,b)=0;
  }
}

size_t plotMinCol(cv::Mat& image,
		const IntVector& min_cols) {
  size_t nnz=0;
  int num_variables=min_cols.size();
  for (size_t r=0; r<num_variables; r++){
    for (size_t c=min_cols[r]; c<=r; c++){
      unsigned char & v=image.at<unsigned char>(r, c);
      if (v==0) {
	nnz++;
	continue;
      }
      bool full=false;
      int start_scan=std::min(min_cols[r], min_cols[c]);
      for(int cc=start_scan; !full && cc<c; cc++){
	unsigned char lc=image.at<unsigned char>(r, cc)<200;
	unsigned char lr=image.at<unsigned char>(c, cc)<200;
	full|= (lc&&lr);
      }
      if (full) {
	nnz++;
	v=127;
      }
    }
  }
  return nnz;
}

inline int generateFactors(FactorVector& factors,
		     size_t num_odometries,
		     size_t num_closures){
  factors.resize(num_odometries+num_closures);
  int num_variables=0;
  // generate the pose chain
  for(size_t i=0; i<num_odometries; i++){
    factors[i].from=num_variables;
    factors[i].to=num_variables+1;
    num_variables++;
  }
  num_variables++;
  size_t k=num_variables;
  for (size_t i=0; i<num_closures; i++){
    int from_idx=floor( drand48()*(num_variables-1));
    int to_idx=floor( drand48()*(num_variables-1));
    if (from_idx==to_idx)
      continue;
    Factor& factor=factors[k];
    factor.from=from_idx;
    factor.to=to_idx;
    k++;
  }
  factors.resize(k);
  return num_variables;
}

inline size_t nonZeroEntries(const IntVector& min_col) {
  size_t nnz=0;
  for (size_t i=0; i<min_col.size(); i++)
    nnz+=i-min_col[i];
  return nnz+min_col.size();
}

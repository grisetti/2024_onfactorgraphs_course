#ifndef INT_PAIR_H
#define INT_PAIR_H
#include <utility>
#include <set>

typedef std::pair<int, int> IntPair;
typedef std::vector<IntPair> IntPairVector;
typedef std::set<IntPair> IntPairSet;

struct IntPairCompare{
  IntPairCompare(bool ordering=true){
    this->ordering=ordering;
  }
  bool operator()(const IntPair& p1, const IntPair& p2) {
    if (ordering) {
      return p1.second<p2.second ||
		       (p1.second==p2.second && p1.first<p2.first);
    } else {
      return p1.second<p2.second ||
		       (p1.second==p2.second && p1.first>p2.first);
    }
  }
  bool ordering;  
};
#endif // INT_PAIR_H

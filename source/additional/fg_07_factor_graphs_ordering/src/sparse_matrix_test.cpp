#include "sparse_matrix.h"
#include <fstream>

using namespace std;

int main(int argc, char** argv){
  if (argc<2){
    cerr << "use " << argv[0] << "<filename>" << endl;
    return 0;
  }
  ifstream is(argv[1]);
  if (! is){
    cerr << "invalid file" << endl;
    return 0;
  }

  SparseMatrix m;
  is >> m;

  cerr << "loaded matrix" << endl;
  cerr << m;

  SparseMatrix chol;

  int nnz=m.cholesky(chol);
  cerr << "nnz= " << nnz << endl;
  cerr << "plain cholesky matrix" << endl;
  cerr << chol;

  nnz=m.choleskyStructure(chol);
  cerr << "nnz= " << nnz << endl;
  cerr << "structure matrix" << endl;
  cerr << chol;

  nnz=m.choleskyRefresh(chol);
  cerr << "nnz= " << nnz << endl;
  cerr << "value matrix" << endl;
  cerr << chol;


  cerr << "matrix: " << endl;
  cerr << m << endl;

  cs* cs_matrix=m.toCS();
  cerr << "converting to cs" << endl;
  
  cerr << "converting from cs" << endl;
  SparseMatrix m2;
  m2.fromCS(cs_matrix);
  cerr << m2 << endl;

}

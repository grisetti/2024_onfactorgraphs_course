#pragma once
#include <vector>
#include <iostream>
#include <cassert>

typedef std::vector<int> IntVector;

inline std::ostream& operator << (std::ostream& os, const IntVector& v){
  for (size_t i=0; i<v.size(); i++)
    os << i << " " << v[i] << " " << std::endl;
  return os;
}

inline void invertPermutation(IntVector& invp, const IntVector& p) {
  invp.resize(p.size());
  for(size_t i=0; i<p.size(); i++)
    invp[p[i]]=i;
}

inline void composePermutation(IntVector& result, const IntVector& p1, const IntVector& p2) {
  assert(p1.size()==p2.size());
  result.resize(p1.size());
  for(size_t i=0; i<p2.size(); i++){
    result[i]=p1[p2[i]];
  }
}

struct IntVectorCompare{
  inline bool operator()(const IntVector& v1, const IntVector& v2) const {
    return std::lexicographical_compare(v1.begin(), v1.end(), v2.begin(), v2.end());
  }
};

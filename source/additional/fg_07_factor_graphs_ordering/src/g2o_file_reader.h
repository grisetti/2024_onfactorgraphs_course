#ifndef G2O_FILE_READER_H
#define G2O_FILE_READER_H

#include <fstream>
#include <iostream>
#include <map>
#include "g2o/core/optimizable_graph.h"
#include "factor.h"
#include "sparse_matrix.h"


typedef std::map<int, int> IntMap;
typedef std::vector<int> IntVector;
typedef std::multimap<int, int> LandmarksMultiMap;

struct G2OFileReader
{
    G2OFileReader(char* input_filename);

    void extractVariablesAndFactors();
    void graph2sm();
    void write(std::ofstream& ofs);

    inline void setEnableSchurComplement(bool& b) { _enable_schur_complement = b; }
    inline int& numVariables() { return _num_variables; }
    inline FactorVector& factors() { return _factors; }
    inline IntMap& idMap() { return _id_map; }
    inline SparseMatrix& sparseMatrix() { return _sm; }

    bool _enable_schur_complement;
    int _num_variables;

    g2o::OptimizableGraph _graph;
    IntVector _unordered_variables;
    IntMap _id_map;
    FactorVector _factors;
    LandmarksMultiMap _landmarks_observations;
    SparseMatrix _sm;
};
#endif // G2O_FILE_READER_H

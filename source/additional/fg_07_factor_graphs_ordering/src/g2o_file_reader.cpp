#include "g2o_file_reader.h"
#include "g2o/types/slam2d/vertex_se2.h"
#include "g2o/types/slam2d/vertex_point_xy.h"
#include "g2o/types/slam2d/edge_se2.h"
#include "g2o/types/slam2d/edge_se2_pointxy.h"


G2OFileReader::G2OFileReader(char* input_filename)
{
    _graph.load(input_filename);

    _enable_schur_complement = false;
    _num_variables = 0;
}


void G2OFileReader::extractVariablesAndFactors()
{
    // First extract the number of variables
    g2o::OptimizableGraph::VertexIDMap& vertices = _graph.vertices();
    for(g2o::OptimizableGraph::VertexIDMap::iterator it = vertices.begin(); it != vertices.end(); ++it)
    {
        g2o::VertexSE2* v = dynamic_cast<g2o::VertexSE2*>(it->second);
        if(v)
        {
            _unordered_variables.push_back(v->id());
        }
        g2o::VertexPointXY* l = dynamic_cast<g2o::VertexPointXY*>(it->second);
        if(l && _enable_schur_complement)
        {
            _unordered_variables.push_back(l->id());
        }
    }

    // Sort variables based on id ascending order
    std::sort(_unordered_variables.begin(), _unordered_variables.end());
    _num_variables = _unordered_variables.size();
    for(size_t i = 0; i < _num_variables; ++i)
    {
        _id_map.insert(std::make_pair(_unordered_variables[i], i));
    }

    // Then extract the edges
    g2o::OptimizableGraph::EdgeSet& edges = _graph.edges();
    for(g2o::OptimizableGraph::EdgeSet::iterator it = edges.begin(); it != edges.end(); ++it)
    {
        g2o::EdgeSE2* edge = dynamic_cast<g2o::EdgeSE2*>(*it);
        if(edge)
        {
            g2o::VertexSE2* v1 = static_cast<g2o::VertexSE2*>(edge->vertices()[0]);
            g2o::VertexSE2* v2 = static_cast<g2o::VertexSE2*>(edge->vertices()[1]);
            IntMap::iterator from_it = _id_map.find(v1->id());
            IntMap::iterator to_it = _id_map.find(v2->id());
            if(from_it != _id_map.end() && to_it != _id_map.end())
            {
                int from_idx = from_it->second;
                int to_idx = to_it->second;
                _factors.push_back(Factor(from_idx, to_idx));
            }
        }
        g2o::EdgeSE2PointXY* edge_xy = dynamic_cast<g2o::EdgeSE2PointXY*>(*it);
        if(edge_xy && _enable_schur_complement)
        {
            g2o::VertexSE2* v = static_cast<g2o::VertexSE2*>(edge_xy->vertices()[0]);
            g2o::VertexPointXY* l = static_cast<g2o::VertexPointXY*>(edge_xy->vertices()[1]);
            IntMap::iterator v_it = _id_map.find(v->id());
            IntMap::iterator l_it = _id_map.find(l->id());
            if(v_it != _id_map.end() && l_it != _id_map.end())
            {
                int v_idx = v_it->second;
                int l_idx = l_it->second;
                std::cerr << "[PRELIMINARY] " << l_idx << " " << v_idx << std::endl;
                _landmarks_observations.insert(std::make_pair(l_idx, v_idx));
            }
        }
    }
    std::cerr << "landmarks observed: " << _landmarks_observations.size() << std::endl;
    if(_enable_schur_complement)
    {
        for(LandmarksMultiMap::iterator it = _landmarks_observations.begin(); it != _landmarks_observations.end(); ++it)
        {
            std::cerr << "[OUTER] " << it->first << " " << it->second << std::endl;
            int cnt = 0;
            std::pair<LandmarksMultiMap::iterator, LandmarksMultiMap::iterator> range = _landmarks_observations.equal_range(it->first);
            for(LandmarksMultiMap::iterator rf = range.first; rf != range.second; ++rf)
            {
                std::cerr << it->first << " " << it->second << "; " << cnt++ << std::endl;
            }
        }
    }
}


void G2OFileReader::graph2sm()
{
    // Allocate and fill the sparse matrix
    _sm.resize(_num_variables, _num_variables);
    size_t size = _factors.size();
    for(size_t i = 0; i < size; ++i)
    {
        Factor& factor = _factors[i];
        int from_idx = factor.from;
        int to_idx = factor.to;
        _sm.setElement(from_idx,from_idx,1);
        _sm.setElement(from_idx,to_idx,1);
        _sm.setElement(to_idx,from_idx,1);
        _sm.setElement(to_idx,to_idx,1);
    }
}


void G2OFileReader::write(std::ofstream& ofs)
{
    ofs << _sm;
}

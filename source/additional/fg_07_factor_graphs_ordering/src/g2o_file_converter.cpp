#include "g2o_file_reader.h"
#include "g2o/types/slam2d/vertex_se2.h"
#include "g2o/types/slam2d/vertex_point_xy.h"
#include "g2o/types/slam2d/edge_se2.h"
#include "g2o/types/slam2d/edge_se2_pointxy.h"


g2o::VertexSE2 vse2;
g2o::VertexPointXY vxy;
g2o::EdgeSE2 ese2;
g2o::EdgeSE2PointXY exy;


int main(int argc, char** argv)
{
    if(argc < 3)
    {
        std::cerr << "[ERROR]: ./g2o_file_converter <input graph> <output file>" << std::endl;
        return -1;
    }
    bool enable_schur = false;
    if(argc == 4)
        enable_schur = argv[3];

    G2OFileReader gfr(argv[1]);
    gfr.setEnableSchurComplement(enable_schur);
    gfr.extractVariablesAndFactors();
    gfr.graph2sm();
    std::ofstream ofs(argv[2]);
    gfr.write(ofs);
    return 0;
}

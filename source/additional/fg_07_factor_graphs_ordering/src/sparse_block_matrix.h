#pragma once
#include <vector>
#include <map>
#include <limits>
#include <stdexcept>
#include <iostream>
#include <cmath>
#include "int_vector.h"
#include "opencv2/opencv.hpp"
#include <suitesparse/cs.h>
#include <Eigen/Core>
#include "sparse_matrix.h"


template <typename BlockType_>
struct SparseBlockMatrix{
  static const int =BlockSize_;
  typedef typename Eigen::Matrix<Scalar, BlockSize_, BlockSize_> Block;
  typedef typename std::map<int, Block> IntBlockMap;
  typedef typename std::vector<int, IntBlockMap> IntBlockMapVector;

  SparseBlockMatrix();
  SparseMatrix(int num_block_rows, int num_block_cols);
  void clear();
  void setElement(int block_r, int block_c, const BlockType& block);
  Block element(int block_r, int block_c) const;
  Block element(int block_r, int block_c) const;
  bool isElement(int block_r, int block_c) const;
  void resize(int new_rows, int new_cols);
  int structure(SparseMatrix& structure_matrix) const;
  int choleskyStructure(SparseMatrix& lower) const;
  int cholesky(SparseBlockMatrix<Scalar_, BlockSize_>& lower) const;
  int choleskyRefresh(SparseBlockMatrix<Scalar_, BlockSize_>& lower) const;
  void computeMinCols(IntVector& min_col_indices);
  void computeBlocksDegree(IntVector& degrees);
  void structureToCvMat(cv::Mat& m, unsigned char gray_level) const;
  int countNonZeros() const;
  IntBlockMapVector rows;
  int num_rows;
  int num_cols;
  
  cs* toCS() const;
  void fromCS(const cs* src);
};

std::ostream& operator <<(std::ostream& os, const SparseMatrix& m);
std::istream& operator >>(std::istream& is, SparseMatrix& m);

#pragma once
#include <vector>
#include <iostream>

struct Factor{
  Factor(int from_=0, int to_=0): from(from_), to(to_){
  }
  int from;
  int to;
  bool operator < (const Factor& other){
    return (from<other.from)||
      ( (from==other.from) && (to<other.to) );
  }
};

typedef std::vector<Factor> FactorVector;

inline std::ostream& operator << (std::ostream& os, const Factor& factor){
  os << factor.from << " -> " << factor.to;
  return os;
}

inline std::ostream& operator << (std::ostream& os, const FactorVector& factors){
  for (size_t i=0; i<factors.size(); i++)
    os << factors[i] << std::endl;
  return os;
}

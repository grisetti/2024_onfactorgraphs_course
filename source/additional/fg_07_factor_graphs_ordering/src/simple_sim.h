#pragma once
#include <cstdlib>
#include "factor.h"

typedef std::multimap<int, int> IntMultiMap;

inline int sampleIndex(float* probs, int num){
  float normalizer=0;
  for (int i=0; i<num; i++){
    normalizer+=probs[i];
  }
  normalizer=1./normalizer;

  float random_value=drand48();
  float cum_value=0;
  for (int i=0; i<num; i++){
    cum_value+=normalizer*probs[i];
    if (random_value<=cum_value)
      return i;
  }
  return num-1;
}

struct SlamPathSimulator{
  SlamPathSimulator(){
    prob_keep=0.95; // probability of remaining in the current state
    prob_reverse=0.1; // probability of changing direction
    prob_jump=0.2; // probabilty of doing a localization jump
    prob_explore=0.0; // probability of entering in an unexplored area (from localization)
    time_index=0;
    location=0;
  }

#define DEBUG(x)\
  if (x==true)\
    std::cerr 

  inline void step(FactorVector& factors) {
    bool enable_shit_print=false;
    IntMultiMap::iterator map_begin=locations.begin();
    IntMultiMap::reverse_iterator map_end=locations.rbegin();
    int map_begin_idx=-1;
    int map_end_idx=-1;

    // avoid segfault at start
    if (time_index>1){
      map_begin_idx=map_begin->first;
      map_end_idx=map_end->first;
    }

    int action=-1;
    DEBUG(enable_shit_print) <<"Previous state [" << location << "," << previous_location <<"]: ";
    if (time_index<2){
      // at the first two time steps we move forward
      DEBUG(enable_shit_print) << "Start";
      action=1;
    } else if (location==map_end_idx) {
      // are we end of the map?
      DEBUG(enable_shit_print) << "EndMap";
      float probs[]={prob_keep, prob_reverse, prob_jump};
      action = sampleIndex(probs, sizeof(probs)/sizeof(float));
    } else if (location==map_begin_idx) {
      DEBUG(enable_shit_print) << "StartMap";
      // are we at the beginning of the map?
      float probs[]={prob_reverse, prob_keep, prob_jump};
      action= sampleIndex(probs, sizeof(probs)/sizeof(float));
    } else if (location-previous_location==1){
      DEBUG(enable_shit_print) << "ForwardMove";
      //are we moving forward?
      float probs[]={prob_keep, prob_reverse, prob_jump};
      action= sampleIndex(probs, sizeof(probs)/sizeof(float));
    } else if (location-previous_location==-1){
      DEBUG(enable_shit_print) << "BackwardMove";
      //are we moving backward?
      float probs[]={prob_reverse, prob_keep, prob_jump, prob_explore, prob_explore};
      action= sampleIndex(probs, sizeof(probs)/sizeof(float));
    } else if (location-previous_location>1){
      DEBUG(enable_shit_print) << "LocalizeForward";
      //are we jumping forward?
      float probs[]={prob_keep, prob_keep, prob_jump, prob_explore, prob_explore};
      action= sampleIndex(probs, sizeof(probs)/sizeof(float));
    } else if (location-previous_location<-1){
      DEBUG(enable_shit_print) << "LocalizeBackward";
      //are we jumping backward?
      float probs[]={prob_keep, prob_keep, prob_jump, prob_explore, prob_explore};
      action= sampleIndex(probs, sizeof(probs)/sizeof(float));
    } 
    DEBUG(enable_shit_print) << std::endl;

    previous_time_index=time_index;
    if (time_index==0){
      locations.insert(std::make_pair(location, time_index));
    }
    previous_location=location;
    time_index++;
    DEBUG(enable_shit_print) << "Action: ";
    switch(action){
    case 0: // move forward
      DEBUG(enable_shit_print) << "Forward ";
      location++;
      break;
    case 1: // move backward
      DEBUG(enable_shit_print) << "Backward ";
      location--;
      break;
    case 2: // relocalize
      DEBUG(enable_shit_print) << "Relocalize ";
      {
	int l=location;
	int l_new=location;
	while(l_new==l) {
	  l_new=drand48()*(map_end_idx-map_begin_idx)+map_begin_idx;
	}
	DEBUG(enable_shit_print) << "l_new: " << l_new << " l: " << l << std::endl;
	location=l_new;
      }
      break;
    case 3: // explore at the end
      DEBUG(enable_shit_print) << "ExploreForward ";
      location=map_end_idx+1;
      break;
    case 4:
      DEBUG(enable_shit_print) << "ExploreBackward ";
      location=map_begin_idx-1;
      break;
    default:
      DEBUG(enable_shit_print) << "Action: " << action << std::endl;
      throw std::runtime_error("illegal action");
    }
    
    Factor current_odom_factor(previous_time_index, time_index);
    factors.push_back(current_odom_factor);

    std::pair<IntMultiMap::iterator, IntMultiMap::iterator> range=locations.equal_range(location);
    int revisits_count=0;
    int skip_closures=1;
    for (IntMultiMap::iterator it=range.first; it!=range.second; it++){
      Factor closure_factor(it->second, time_index);
      if (! (revisits_count%skip_closures))
	  factors.push_back(closure_factor);
      revisits_count++;
    }

    locations.insert(std::make_pair(location, time_index));
    DEBUG(enable_shit_print) << ", Time Index: " << time_index;
    DEBUG(enable_shit_print) << ", Location: " << location;
    DEBUG(enable_shit_print) << ", Revisits: " << revisits_count;
    DEBUG(enable_shit_print) << std::endl;
  }


  int time_index;
  int previous_time_index;
  int location;
  int previous_location;
  IntMultiMap locations;

  float prob_keep;
  float prob_reverse;
  float prob_jump;
  float prob_explore;
};

#include "orderings.h"


void computeAMDPermutation(IntVector& amd_permutation,
			   const SparseMatrix& system_matrix){
  cs* cs_matrix=system_matrix.toCS();
  int* amd_ordering=cs_amd(1,cs_matrix);
  amd_permutation.resize(system_matrix.num_rows);
  for (int i=0; i<system_matrix.num_rows; i++){
    amd_permutation[amd_ordering[i]]=i;
  }
  cs_spfree(cs_matrix);
  cs_free(amd_ordering);
}


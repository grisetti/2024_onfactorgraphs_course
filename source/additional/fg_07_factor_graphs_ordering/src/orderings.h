#pragma once
#include "slam_path_simulator.h"
#include "sparse_matrix.h"
#include "factor.h"
#include "int_pair.h"
#include "int_vector.h"
#include <memory>


void computeAMDPermutation(IntVector& amd_permutation,
			   const SparseMatrix& system_matrix);

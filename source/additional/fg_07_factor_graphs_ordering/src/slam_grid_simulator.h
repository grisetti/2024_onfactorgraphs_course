#pragma once
#include "factor.h"
#include "int_pair.h"
#include <map>
#include "opencv2/opencv.hpp"
#include "int_vector.h"

typedef std::multimap<IntPair, int, IntPairCompare> IntPairMultiMap;

struct SlamGridSimulator{
  SlamGridSimulator(int tile_size, int x_tiles, int y_tiles);

  void step(FactorVector& factors);
  
  void turn(bool true_for_left_false_for_right);
  
  int time_index;
  int previous_time_index;
  IntPair location; // location on the grid
  int x_increment;  // int increment on x (direction);
  int y_increment;  // int increment on y (direction);
  void toCvImage(cv::Mat& image, int scale=1);


  IntPair previous_location;
  IntPairMultiMap locations;
  int x_size;
  int y_size;
  int tile_size;
  int distance_traveled_in_current_tile;
  float prob_turn; // probability of turning
  bool keep_only_last_factor;
  bool keep_only_first_factor;

  void grassFireOrdering(IntVector& ordering);
  void computeNeighbors(IntPairVector& neighbors, const IntPair& cell, const IntPairSet& visited);
};

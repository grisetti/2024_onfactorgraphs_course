#pragma once
#include "factor.h"
#include <map>

typedef std::multimap<int, int> IntMultiMap;

struct SlamPathSimulator{
  SlamPathSimulator();

  void step(FactorVector& factors);
  
  int time_index;
  int previous_time_index;
  int location;
  int previous_location;
  IntMultiMap locations;

  float prob_keep; // probability of remaining in the current state
  float prob_reverse; // probability of changing direction
  float prob_jump; // probabilty of doing a localization jump
  float prob_explore; // probability of entering in an unexplored area (from localization)
};

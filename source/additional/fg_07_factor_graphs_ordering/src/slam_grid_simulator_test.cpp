#include "slam_grid_simulator.h"
#include "orderings.h"
#include "sparse_matrix.h"
#include <iostream>
#include "quotient_graph.h"

using namespace std;

inline void fillMatrix(SparseMatrix& dest,
		       const FactorVector& factors, 
		       size_t num_variables,
		       const IntVector& permutation=IntVector()) {
  dest.clear();
  dest.resize(num_variables, num_variables);
  for (size_t i=0; i<factors.size(); i++){
    const Factor& factor=factors[i];
    int from_idx=factor.from;
    int to_idx=factor.to;
    if(permutation.size()){
      from_idx=permutation[from_idx];
      to_idx=permutation[to_idx];
    }
    int a=std::min(from_idx, to_idx);
    int b=std::max(from_idx, to_idx);
    dest.setElement(a,a,1);
    dest.setElement(a,b,1);
    dest.setElement(b,a,1);
    dest.setElement(b,b,1);
  }
}

int compareVectors(IntVector& before, IntVector& after) {
  int min_size=std::min(before.size(), after.size());
  for (int i=0; i<min_size; ++i) {
    if(before[i]!=after[i])
      return i;
  }
  return min_size;
}

int main(int argc, char** argv){
  bool do_plain=true;
  bool do_amd=true;
  bool do_emd=true;
  bool do_plotting=true;

  if (argc<5){
    cerr << " usage:" << argv[0] << " <num_steps> <x_tiles> <y_tiles> <tile_size> <prob_turn>" << endl;
    return 0;
  }
  int num_steps=atoi(argv[1]);
  int x_tiles=atoi(argv[2]);
  int y_tiles=atoi(argv[3]);
  int tile_size=atoi(argv[4]);
  float prob_turn=atof(argv[5]);

  cerr << "running with parameters" << endl;
  cerr << "steps: " << num_steps << endl;
  cerr << "x_tiles: " << x_tiles << endl;
  cerr << "y_tiles: " << y_tiles << endl;
  cerr << "tile_size: " << tile_size << endl;
  cerr << "prob_turn: " << prob_turn << endl;
  

  SlamGridSimulator simulator(tile_size, x_tiles, y_tiles);
  simulator.prob_turn=prob_turn;
  simulator.keep_only_last_factor=false;
  simulator.keep_only_first_factor=true;
  if (do_plotting)
    cv::namedWindow("world");



  cv::Mat plain_system_image(num_steps, num_steps, CV_8UC1);
  if (do_plain && do_plotting)
    cv::namedWindow("plain");


  cv::Mat amd_system_image(num_steps, num_steps, CV_8UC1);
  if (do_amd && do_plotting)
    cv::namedWindow("amd");

  cv::Mat emd_system_image(num_steps, num_steps, CV_8UC1);
  if (do_emd  && do_plotting)
    cv::namedWindow("emd");

  IntVector amd_before;
  IntVector emd_before;

  int plot_steps=10;
  FactorVector factors;
  for (int i=0; i<num_steps; i++){
    simulator.step(factors);
    if (i%plot_steps)
      continue;
    IntVector grassfire_ordering;
    simulator.grassFireOrdering(grassfire_ordering);
    cv::Mat image;
    simulator.toCvImage(image,3);
    if (do_plotting)
      cv::imshow("world", image);

    int num_variables=simulator.time_index+1;
    cerr << "Factors generated: " << factors.size()
	 << " Num_variables: " << num_variables << endl;;

    IntVector plain_min_col, plain_degree;
    SparseMatrix plain_matrix;
    fillMatrix(plain_matrix,factors,num_variables);
    plain_matrix.computeMinCols(plain_min_col);
    plain_matrix.computeDegree(plain_degree);
    

    /***************  PLAIN VERSION ************************/
    if(do_plain) {
      cerr << "********* PLAIN **********" << endl;
      cerr << "nnz:" << plain_matrix.countNonZeros() << endl;
      plain_system_image=cv::Scalar(255);
      plain_matrix.structureToCvMat(plain_system_image,0);
      SparseMatrix plain_chol;
      plain_matrix.choleskyStructure(plain_chol);
      cerr << "chol nnz:" << plain_chol.countNonZeros() << endl;
      if (do_plotting){
	plain_chol.structureToCvMat(plain_system_image,125);
	cv::imshow("plain", plain_system_image);
      }
    }


    if (do_emd){
      /***************  RETARD VERSION ************************/
      IntVector emd_permutation;
      QuotientGraph graph(factors, num_variables);
      graph.setPolicy(QuotientGraph::Policy::Approximate);
      graph.mdo(emd_permutation);
      
      SparseMatrix emd_matrix;
      cerr << "********* EMD **********" << endl;
      fillMatrix(emd_matrix,factors,num_variables,emd_permutation); 
      cerr << "nnz:" << emd_matrix.countNonZeros() << endl;
      emd_system_image=cv::Scalar(255);
      emd_matrix.structureToCvMat(emd_system_image,0);
      SparseMatrix emd_chol;
      emd_matrix.choleskyStructure(emd_chol);
      cerr << "chol nnz:" << emd_chol.countNonZeros() << endl;
      if (do_plotting){
	emd_chol.structureToCvMat(emd_system_image,125);
	cv::imshow("emd", emd_system_image);
      }
      cerr << "difference start" << compareVectors(emd_before, emd_permutation) << endl;
      emd_before=emd_permutation;
     
    }



    if (do_amd){
      /***************  AMD VERSION ************************/
      IntVector amd_permutation;
      computeAMDPermutation(amd_permutation, plain_matrix);
      
      SparseMatrix amd_matrix;
      cerr << "********* AMD **********" << endl;
      fillMatrix(amd_matrix,factors,num_variables,amd_permutation);
      cerr << "nnz:" << amd_matrix.countNonZeros() << endl;
      amd_system_image=cv::Scalar(255);
      amd_matrix.structureToCvMat(amd_system_image,0);
      SparseMatrix amd_chol;
      amd_matrix.choleskyStructure(amd_chol);
      cerr << "chol nnz:" << amd_chol.countNonZeros() << endl;
      if (do_plotting){
       amd_chol.structureToCvMat(amd_system_image,125);
       cv::imshow("amd", amd_system_image);
      }
      
      cerr << "difference start" << compareVectors(amd_before, amd_permutation) << endl;
      amd_before=amd_permutation;
    }
    cv::waitKey(0);

  }
  if (do_plotting)
    cv::waitKey(0);

}

#include <cstdlib>
#include "slam_grid_simulator.h"
#include "sample_index.h"
#include <stdexcept>
#include <deque>
#include <list>

SlamGridSimulator::SlamGridSimulator(int tile_size, int x_tiles, int y_tiles){
  this->tile_size=tile_size;
  this->x_size=x_tiles*tile_size;
  this->y_size=y_tiles*tile_size;
  prob_turn=0.5; // probability of remaining in the current state
  time_index=0;
  location=IntPair(x_size/2, y_size/2);
  distance_traveled_in_current_tile=0;
  x_increment=1;
  y_increment=0;
  previous_time_index=0;
  previous_location=location;
  keep_only_last_factor=false;
  distance_traveled_in_current_tile=0;
}


void SlamGridSimulator::turn(bool true_for_left_false_for_right){
  int x_inc=0, y_inc=0;
  if (true_for_left_false_for_right){
    x_inc=-y_increment;
    y_inc=x_increment;
  } else {
    x_inc=y_increment;
    y_inc=-x_increment;
  }
  x_increment=x_inc;
  y_increment=y_inc;
}

void SlamGridSimulator::step(FactorVector& factors) {
  previous_location=location;
  previous_time_index=time_index;
  if (time_index==0){
    locations.insert(std::make_pair(location, time_index));
  }
  time_index++;
  // action 
  // 0: forward (applies x_inc, and y_inc to location)
  // 1: turn left
  // 2: turn right
  int action=-1;
  float probs[]={1-prob_turn, .5f*prob_turn, .5f*prob_turn};
  int current_x=location.first;
  int current_y=location.second;
  if (distance_traveled_in_current_tile<tile_size){
    action=0;
    ++distance_traveled_in_current_tile;
  } 
 

  if (action<0){
    action=sampleIndex(probs,3);
    distance_traveled_in_current_tile=0;
  }

  int new_x=current_x+x_increment;
  int new_y=current_y+y_increment;
  switch(action){
  case 0: 
    if (new_x<0) {
      new_x=0;
      x_increment=1;
      y_increment=0;
      distance_traveled_in_current_tile=0;
    } else if(new_x>x_size-1) {
      new_x=x_size-1;
      x_increment=-1;
      y_increment=0;
      distance_traveled_in_current_tile=1;
    } else if (new_y<0) {
      new_y=0;
      y_increment=1;
      x_increment=0;
      distance_traveled_in_current_tile=0;
    } else if(new_y>y_size-1) {
      new_y=y_size-1;
      y_increment=-1;
      x_increment=0;
      distance_traveled_in_current_tile=1;
    } else {
      distance_traveled_in_current_tile++;
    } 
    location.first=new_x;
    location.second=new_y;
    break;
  case 1:
    turn(true);
    break;
  case 2:
    turn(false);
    break;
  default:
      throw std::runtime_error("mannagga il porco");
  }
    
  Factor current_odom_factor(previous_time_index, time_index);
  factors.push_back(current_odom_factor);

  std::pair<IntPairMultiMap::iterator, IntPairMultiMap::iterator> range=locations.equal_range(location);
  int revisits_count=0;
  int skip_closures=1;
  Factor last_factor;
  for (IntPairMultiMap::iterator it=range.first; it!=range.second; it++){
    Factor closure_factor(it->second, time_index);
    last_factor=closure_factor;
    if (! keep_only_last_factor && ! (revisits_count%skip_closures))
      factors.push_back(closure_factor);
    revisits_count++;
    if (keep_only_first_factor)
      break;
  }
  if (keep_only_last_factor&&revisits_count)
    factors.push_back(last_factor);

  locations.insert(std::make_pair(location, time_index));
}

void SlamGridSimulator::toCvImage(cv::Mat& image, int scale){
  image.create(x_size*scale, y_size*scale, CV_8UC1);
  image=cv::Scalar(255);
  for(IntPairMultiMap::const_iterator it=locations.begin(); it!=locations.end(); it++){
    const IntPair location=it->first;
    int x=location.first*scale;
    int y=location.second*scale;
    unsigned char& cell=image.at<unsigned char>(x,y);
    cell=0;
  }
}


void SlamGridSimulator::computeNeighbors(IntPairVector& neighbors, const IntPair& cell, const IntPairSet& visited){
  int x=cell.first;
  int y=cell.second;
  
  IntPair left(x-1,y);
  IntPair right(x+1,y);
  IntPair up(x,y-1);
  IntPair down(x,y+1);

  neighbors.clear();

  if (locations.find(left)!=locations.end()&&
      visited.find(left)==visited.end())
    neighbors.push_back(left);

  if (locations.find(right)!=locations.end()&&
      visited.find(right)==visited.end())
    neighbors.push_back(right);

  if (locations.find(up)!=locations.end()&&
      visited.find(up)==visited.end())
    neighbors.push_back(up);

  if (locations.find(down)!=locations.end()&&
      visited.find(down)==visited.end())
    neighbors.push_back(down);
}

using namespace std;

typedef std::map<IntPair, IntPairVector> IntPairIntPairVectorMap;
typedef std::map<IntPair, IntPair> IntPairIntPairMap;
typedef std::list<IntPair> IntPairList;

void traverseTree(IntPairList& visit_order, 
		  const IntPair& node, 
		  const IntPairIntPairVectorMap& directed_tree){
  visit_order.push_back(node);
  IntPairIntPairVectorMap::const_iterator dest_it=directed_tree.find(node);
  if (dest_it==directed_tree.end())
    return;
  const IntPairVector& children=dest_it->second;
  //cerr << "n: (" << node.first << "," << node.second << ")-> children: ";
  //cerr << "children.size: " << children.size() << endl;
  for (size_t i=0; i<children.size(); i++){
    //cerr << "(" << children[i].first << "," << children[i].second << ") ";
    traverseTree(visit_order, children[i], directed_tree);
  }
}

void SlamGridSimulator::grassFireOrdering(IntVector& ordering){
  IntPair start_cell(x_size/2, y_size/2);
  IntPairSet visited;
  IntPairIntPairMap parent_map;
  std::deque<IntPair> frontier;
  frontier.push_back(start_cell);
  visited.insert(start_cell);
  while(! frontier.empty()){
    IntPair current=frontier.front();
    frontier.pop_front();
    IntPairVector neighbors;
    computeNeighbors(neighbors, current, visited);
    for (size_t i=0; i<neighbors.size(); i++){
      const IntPair expanded=neighbors[i];
      parent_map.insert(std::make_pair(expanded, current));
      visited.insert(expanded);
      frontier.push_back(expanded);
    }
  }
  
  std::cerr << "visited.size: " << visited.size() << std::endl;

  // reconstruct the directed tree
  IntPairIntPairVectorMap directed_tree;
  for (IntPairIntPairMap::iterator it=parent_map.begin(); it!=parent_map.end(); it++){
    IntPair child=it->first;
    IntPair parent=it->second;
    //cerr << "parent: (" << parent.first << "," << parent.second << ")";
    //cerr << "child: (" << child.first << "," << child.second << ")" << endl;
  
    IntPairIntPairVectorMap::iterator dest_it=directed_tree.find(parent);
    if (dest_it==directed_tree.end()){
      IntPairVector children;
      children.push_back(child);
      directed_tree.insert(std::make_pair(parent, children));
    } else {
      dest_it->second.push_back(child);
    }
  }

  for (IntPairIntPairVectorMap::iterator it=directed_tree.begin(); it!=directed_tree.end(); it++){
    IntPair node=it->first;
    const IntPairVector& children=it->second;
    //cerr << "node: (" << node.first << "," << node.second << ")->";
    for (size_t i=0; i<children.size(); i++){
      const IntPair& child=children[i];
      //cerr << "child: (" << child.first << "," << child.second << ")";
    }
    //cerr << endl;
  }

  std::cerr << "reconstructed.size: " << directed_tree.size() << std::endl;
 
  // visit the tree
  IntPairList visit_order;
  traverseTree(visit_order, start_cell, directed_tree);
  //cerr << "traversed_size:" << visit_order.size() << endl;


  int num_variables=time_index+1;
  ordering.resize(time_index+1);
  int k=0;
  for (IntPairList::reverse_iterator it=visit_order.rbegin(); it!=visit_order.rend(); it++){
    const IntPair& location=*it;


    std::pair<IntPairMultiMap::iterator, IntPairMultiMap::iterator> range=locations.equal_range(location);
    for (IntPairMultiMap::iterator it=range.first; it!=range.second; it++){
      int var_idx=it->second;
      ordering[var_idx]=k;
      k++;
    }
  }
  cerr << "reconstructed vars: " << k << endl;
}

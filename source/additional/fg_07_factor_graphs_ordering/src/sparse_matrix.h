#pragma once
#include <vector>
#include <map>
#include <limits>
#include <stdexcept>
#include <iostream>
#include <cmath>
#include "int_vector.h"
#include "opencv2/opencv.hpp"
#include <suitesparse/cs.h>

typedef  std::map<int, float> IntFloatMap;
typedef std::vector<IntFloatMap> IntFloatMapVector;

struct SparseMatrix{
  SparseMatrix();
  SparseMatrix(int num_rows, int num_cols);
  void clear();
  void setElement(int r, int c, float value);
  float element(int r, int c) const;
  bool isElement(int r, int c) const;
  void resize(int new_rows, int new_cols);
  int choleskyStructure(SparseMatrix& lower) const;
  int cholesky(SparseMatrix& lower) const;
  int choleskyRefresh(SparseMatrix& lower) const;
  void computeMinCols(IntVector& min_col_indices);
  void computeDegree(IntVector& degrees, int min_degree=0);
  void structureToCvMat(cv::Mat& m, unsigned char gray_level) const;
  int countNonZeros() const;
  IntFloatMapVector rows;
  int num_rows;
  int num_cols;
  cs* toCS() const;
  void fromCS(const cs* src);
};

std::ostream& operator <<(std::ostream& os, const SparseMatrix& m);
std::istream& operator >>(std::istream& is, SparseMatrix& m);

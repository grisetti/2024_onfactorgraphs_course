#include "sample_index.h"
#include <cstdlib>

int sampleIndex(float* probs, int num){
  float normalizer=0;
  for (int i=0; i<num; i++){
    normalizer+=probs[i];
  }
  normalizer=1./normalizer;

  float random_value=drand48();
  float cum_value=0;
  for (int i=0; i<num; i++){
    cum_value+=normalizer*probs[i];
    if (random_value<=cum_value)
      return i;
  }
  return num-1;
}

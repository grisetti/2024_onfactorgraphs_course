#include <vector>
#include <queue>
#include <limits>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;
template <typename CellType_>
struct Grid_ {
  using CellType=CellType_;
  using ContainerType=std::vector<CellType>;
  int rows;
  int cols;
  ContainerType cells;
  Grid_(int r, int c):
    rows(r),
    cols(c),
    cells(r*c){}

  inline CellType& at(int r, int c){
    return cells[r*cols+c];
  };

  inline const CellType& at(int r, int c) const {
    return cells[r*cols+c];
  }

  inline const bool inside (int r, int c) const {
    return r>=0 && r<rows && c>=0 && c<cols;
  }
  
  inline std::pair<int, int> ptr2idx(const CellType* c) const {
      const int offset=c-cells[0];
      return std::make_pair(offset/cols, offset%cols);
  }
};
  
struct DMapCell {
  DMapCell* parent=this;
  int distance2=std::numeric_limits<int>::max();
};

struct DMap {
  using DMapCellVector=std::vector<DMapCell>;
  DMap(int r, int c):
    rows(r),
    cols(c),
    cells(r*c){}
  
  inline DMapCell& at(int r, int c) {
    return cells[r*cols+c];
  }

  inline const DMapCell& at(int r, int c) const {
    return cells[r*cols+c];
  }
  
  inline bool inside(int r, int c) const {
    int offset=r*cols+c;
    return offset>=0 && offset<cells.size();
  }
  std::pair<int,int> ptr2idx(const DMapCell* c) const {
    int offset=c-&cells[0];
    return std::pair<int,int>(offset/cols, offset%cols);
  }
  int compute(const std::vector<std::pair<int,int>>& obstacles, int d2_max=std::numeric_limits<int>::max()) {
    //1 populate the frontier
    std::queue<DMapCell*> frontier;
    
    for (const auto& p:obstacles) {
      int r=p.first;
      int c=p.second;
      if (!inside(r,c))
        continue;
      auto& cell = at(r,c);
      cell.distance2=0;
      cell.parent=&cell;
      auto node_pos=ptr2idx(&cell);
      frontier.push(&cell);
    }
    //2. expand
    int steps=0;
    while (! frontier.empty()) {
      ++steps;
      auto node=frontier.front();
      frontier.pop();
      auto node_pos=ptr2idx(node);
      int node_r=node_pos.first;
      int node_c=node_pos.second;
      auto ancestor=node->parent;
      auto ancestor_pos=ptr2idx(ancestor);
      int ancestor_r=ancestor_pos.first;
      int ancestor_c=ancestor_pos.second;
      // cout << "node: " << node_r << " " << node_c << " " 
      //      << "ancestor: " << ancestor_r << " " << ancestor_c << " "
      //      << "chidlren: [ ";
      for (int rr=node_r-1; rr <= node_r+1; ++rr) {
        for (int cc=node_c-1; cc <= node_c+1; ++cc) {
          if (rr==node_r && cc==node_c)
            continue;
          if (!inside(rr,cc))
            continue;
          auto& child=at(rr,cc);
          int dr=rr-ancestor_r;
          int dc=cc-ancestor_c;
          int d2=dr*dr+dc*dc;
          if (d2>d2_max)
            continue;
          // cout << "(" << rr << "," << cc << ","<< d2 << "," << child.distance2 <<")";
          if (d2<child.distance2){
            //cout << "P ";
            child.distance2=d2;
            child.parent=ancestor;
            frontier.push(&child);
          }
        }
      }
      //cout << "]" << endl;
      // we consider all neightbors of 
    }
    return steps;
  }
  void clear(int d2_max=std::numeric_limits<int>::max()) {
    DMapCell empty;
    empty.distance2=d2_max;
    std::fill(cells.begin(), cells.end(), empty);
  }
  int rows;
  int cols;
  DMapCellVector cells;

};

int main(int argc, char** argv) {
  if (argc<6) {
    std::cout << "usage " << argv[0] << " <rows> <cols> <fill_in> <dmax> <output>" << std::endl;
    return -1;
  }
  
  int rows=atoi(argv[1]);
  int cols=atoi(argv[2]);
  float fill_in=atof(argv[3]);
  int dmax=atoi(argv[4]);
  dmax *= dmax;
  string filename =argv[5];

  DMap dmap(rows, cols);
  int size=rows*cols;
  int num_obstacles=size*fill_in;
  std::vector<std::pair<int,int>> obstacles(num_obstacles);
  for (auto& o: obstacles) {
    o.first=drand48()*rows;
    o.second=drand48()*cols;
  }
  int ops;
  dmap.clear(dmax);
  ops = dmap.compute(obstacles, dmax);
  cout << "ops: " << ops << endl;
  ofstream os (filename);
  for (int r=0; r<dmap.rows; ++r) {
    for (int c=0; c<dmap.cols; ++c){
      const auto& cell=dmap.at(r,c);
      os << sqrt(cell.distance2) << " ";
    }
    os << endl;
  }
}

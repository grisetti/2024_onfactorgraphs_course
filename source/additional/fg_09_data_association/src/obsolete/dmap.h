#pragma once
#include "grid.h"
#include <limits>

using namespace std;

//distance map cell
//it has a (squared) distance to the parent, and a pointer to the parent 
struct DMapCell {
  DMapCell* parent=this;
  int distance2=std::numeric_limits<int>::max();
};

struct DMap: public Grid_<DMapCell> {

  DMap(int r, int c):
    Grid_<DMapCell>(r,c){}

  int compute(const std::vector<std::pair<int,int>>& obstacles,
              int d2_max=std::numeric_limits<int>::max());
  
  void clear(int d2_max=std::numeric_limits<int>::max());

  template <typename OutputCellType>
  void copyTo(Grid_<OutputCellType>& dest, int dmax2=std::numeric_limits<int>::max()) {
    dest.resize(rows, cols);
    for (size_t i=0; i<cells.size(); ++i)
      dest.cells[i]=std::min(cells[i].distance2, dmax2);
  }
  
};

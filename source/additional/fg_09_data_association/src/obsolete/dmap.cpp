#include "dmap.h"
#include <queue>

using namespace std;
  
int DMap::compute(const std::vector<std::pair<int,int>>& obstacles, int d2_max) {
  //1 populate the frontier
  std::queue<DMapCell*> frontier;
    
  for (const auto& p:obstacles) {
    int r=p.first;
    int c=p.second;
    if (!inside(r,c))
      continue;
    auto& cell = at(r,c);
    cell.distance2=0;
    cell.parent=&cell;
    auto node_pos=ptr2idx(&cell);
    frontier.push(&cell);
  }
  //2. expand
  int steps=0;
  while (! frontier.empty()) {
    ++steps;
    // fetch the node
    auto node=frontier.front();
    frontier.pop();

    // fetch the position
    auto node_pos=ptr2idx(node);
    int node_r=node_pos.first;
    int node_c=node_pos.second;
    
    // fetch the parent of the node
    auto ancestor=node->parent;
    auto ancestor_pos=ptr2idx(ancestor);
    int ancestor_r=ancestor_pos.first;
    int ancestor_c=ancestor_pos.second;

    // visit all neighbors
    for (int rr=node_r-1; rr <= node_r+1; ++rr) {
      for (int cc=node_c-1; cc <= node_c+1; ++cc) {
        if (rr==node_r && cc==node_c)
          continue;
        if (!inside(rr,cc))
          continue;
        auto& child=at(rr,cc);
        int dr=rr-ancestor_r;
        int dc=cc-ancestor_c;
        //compute the squared distance
        int d2=dr*dr+dc*dc;
        if (d2>d2_max)
          continue;
        // if new distance better than previous one, push back
        if (d2<child.distance2){
          child.distance2=d2;
          child.parent=ancestor;
          frontier.push(&child);
        }
      }
    }
  }
  return steps;
}

void DMap::clear(int d2_max) {
  DMapCell empty;
  empty.distance2=d2_max;
  fill(empty);
}
